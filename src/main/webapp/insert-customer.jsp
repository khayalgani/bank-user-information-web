<%--
  Created by IntelliJ IDEA.
  User: Qaniev-PC
  Date: 5/20/2024
  Time: 9:39 PM
  To change this template use File | Settings | File Templates.
--%>
<%@ page contentType="text/html;charset=UTF-8" language="java" %>
<html>
<head>
    <title>Title</title>
    <link href="assets/css/common.css" rel="stylesheet" type="text/css">

</head>
<body>
<div class="add">
    <form action="customer-servlet" method="post">
        <div class="row">
            <div class="left"><label for="name">name</label></div>
            <div class="right"><input type="text" name="name" id="name"></div>
        </div>
        <div class="row">
            <div class="left"><label for="surname">surname</label></div>
            <div class="right"><input type="text" name="surname" id="surname"></div>
        </div>
        <div class="row">
            <div class="left"><label for="fin">fin</label></div>
            <div class="right"><input type="text" name="fin" id="fin"></div>
        </div>
        <div class="row">
            <div class="left"><label for="email">email</label></div>
            <div class="right"><input type="text" name="email" id="email"></div>
        </div>
        <div class="row">
            <div class="left"><label for="phoneNumber">phoneNumber</label></div>
            <div class="right"><input type="text" name="phoneNumber" id="phoneNumber"></div>
        </div>
        <div class="row">
            <div class="right"><input type="submit" name="add-user" value="add-user"></div>
        </div>

    </form>
</div>

</body>
</html>
