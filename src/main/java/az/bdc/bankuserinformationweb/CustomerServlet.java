package az.bdc.bankuserinformationweb;

import java.io.*;
import java.util.List;

import az.bdc.management.dao.User;
import az.bdc.management.service.UserService;
import az.bdc.management.service.impl.UserServiceImpl;
import jakarta.servlet.ServletException;
import jakarta.servlet.http.*;
import jakarta.servlet.annotation.*;

@WebServlet(name = "customerServlet", value = "/customer-servlet")
public class CustomerServlet extends HttpServlet {

    public void init() {

    }

    public void doGet(HttpServletRequest request, HttpServletResponse response) throws IOException {
        response.setContentType("text/html");

        UserService userService = new UserServiceImpl();
        List<User> all = userService.getAll();
        // Hello
        PrintWriter out = response.getWriter();
        out.println("<html><body>");

        all.forEach(p -> {
            out.println("<br>" + p.getName() + " " + p.getSurname() + "<br>"
                    + p.getEmail() + "<br>" + p.getPhone_number() + "<br>");
        });

        out.println("<br> <a href=\"index.jsp\">Back</a> </body> </html>");
    }

    protected void doPost(HttpServletRequest req, HttpServletResponse resp) throws ServletException, IOException {
        String name = req.getParameter("name");
        String surname = req.getParameter("surname");
        String fin = req.getParameter("fin");
        String email = req.getParameter("email");
        String phoneNumber = req.getParameter("phoneNumber");

        UserService userService = new UserServiceImpl();
        User user = new User();
        user.setName(name);
        user.setSurname(surname);
        user.setFin(fin);
        user.setEmail(email);
        user.setPhone_number(phoneNumber);
        userService.insert(user);

        resp.sendRedirect("index.jsp");
    }


    public void destroy() {
    }
}